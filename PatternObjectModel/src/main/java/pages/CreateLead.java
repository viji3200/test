package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	
	public MyHomePage clickCreateLead() 
	{
		WebElement  crelead = locateElement("link","CRM/SFA" );
		click(crelead);
		return new MyHomePage();
		
		
		
	}
	
	}