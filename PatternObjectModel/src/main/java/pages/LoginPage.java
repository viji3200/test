package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{

	public LoginPage() {
		PageFactory.initElements(driver, this);
	} 
	
	@FindBy(id="username")
	WebElement eleUserName;
	@Given("Enter the username as (.*)")
	public LoginPage typeUsername(String data) {
		// WebElement eleUserName = locateElement("id", "username");
	     type(eleUserName, data);  
	     return this;
	}
	@FindBy(id="password")
	WebElement elePassWord;
	@Given("Enter the password as (.*)")
	public LoginPage typePassword(String data) {
		//WebElement elePassWord = locateElement("id", "password");
	    type(elePassWord, data);
	    return this;
	}
	@FindBy(className="decorativeSubmit") WebElement eleLogin;
	@When("Click the Loginbutton")
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleLogin);
	  //  HomePage page = new HomePage();
	    return new HomePage();
	 }
	
	
}







