package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	
	@When("Click the Logoutbutton")
	public LoginPage clickLogOut() {
		WebElement eleLogOut = locateElement("class", "decorativeSubmit");
	    click(eleLogOut);  
	    return new LoginPage();
	}
	
}







