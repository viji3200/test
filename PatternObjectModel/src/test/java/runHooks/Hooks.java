package runHooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{

	@Before
	public void beforeScenario(Scenario Sc) {
		System.out.println("Scenatio:"+Sc.getName());
		System.out.println("Scenatio:"+Sc.getId());
		startResult();
		startTestModule(Sc.getName(),Sc.getId());
		test = startTestCase(Sc.getId());
		test.assignCategory("Smoke");
		test.assignAuthor("Vijayan");
		startApp("chrome", "http://leaftaps.com/opentaps");
	}
	
	@After
	public void afterScenario(Scenario Sc) {
		System.out.println("Scenario Status:"+Sc.getStatus());
		closeAllBrowsers();
		endResult();
	}
}
