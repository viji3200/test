package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		//locate the future file
		features="src/test/java/feature/createLead.feature",
		//locate the steps and give only package file name)
		glue= {"pages", "runHooks"}, 
		// to get the snippets, outfile names will be in capes
		dryRun=false,
		snippets=SnippetType.CAMELCASE,
		//to avoid junks characters in console
		monochrome=true,
		//tags will used to run the specific scenario
		tags="@smoke")


public class CreateLead {

}
